/*global cordova, module*/

module.exports = {
    greet: function (name, successCallback, errorCallback) {
    	console.log("Calling greet from custom plugin");
        cordova.exec(successCallback, errorCallback, "Hello", "greet", [name]);
    },
    
    getImeiNumber: function (successCallback, errorCallback) {
    	console.log("Calling getImeiNumber from custom plugin");
        cordova.exec(successCallback, errorCallback, "Hello", "getImeiNumber");
    },

    getPublicDirectory: function (successCallback, errorCallback) {
        console.log("Calling getPublicDirectory from custom plugin");
        cordova.exec(successCallback, errorCallback, "Hello", "getPublicDirectory");
    },

    mergeFiles: function (pathToEncryptedFile, pathToDecryptedFile, successCallback, errorCallback) {
        console.log("Calling mergeFiles from custom plugin");
        cordova.exec(successCallback, errorCallback, "Hello", "mergeFiles", [pathToEncryptedFile, pathToDecryptedFile]);
    },

    decryptFile: function (key, pathToEncryptedFile, pathToDecryptedFile, successCallback, errorCallback) {
        console.log("Calling decryptFile from custom plugin");
        cordova.exec(successCallback, errorCallback, "Hello", "decryptFile", [key, pathToEncryptedFile, pathToDecryptedFile]);
    },

    changeOrientation: function (currentOrientation, successCallback, errorCallback) {
        console.log("Calling changeOrientation from custom plugin: "+currentOrientation);
        cordova.exec(successCallback, errorCallback, "Hello", "changeOrientation", [currentOrientation]);
    },
    
    isRooted: function (successCallback, errorCallback) {
        console.log("Calling isRooted from custom plugin: ");
        cordova.exec(successCallback, errorCallback, "Hello", "isRooted", []);
    }
};