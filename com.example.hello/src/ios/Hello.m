#import "Hello.h"
#import <Cordova/CDV.h>

@implementation Hello

- (void)greet:(CDVInvokedUrlCommand*)command
{
    
    NSString* name = [[command arguments] objectAtIndex:0];
    NSString* msg = [NSString stringWithFormat: @"Hello, %@", name];
    
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsString:msg];
    
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}
- (NSString*)getImeiNumber:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Trying to get the unique device id from custom plugin");
    NSString* UDID =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsString:UDID];
    
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}
- (void)mergeFiles:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* inputFilePath = [command.arguments objectAtIndex:0];
    NSString* outFilePath = [command.arguments objectAtIndex:1];
    
    NSString* resultOk = @"OK";
    NSString* resultFAIL = @"FAIL";
    int status =0;
    if (inputFilePath == nil || [inputFilePath length] == 0 || outFilePath == nil || [outFilePath length] == 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    } else {
        
        NSFileHandle *inputFileHandle, * outputFileHandle;
        @try {
            NSFileManager *fileMgr;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
            NSString* rootPath = paths[0];
            NSString* path = [rootPath stringByAppendingPathComponent:inputFilePath];
            
            NSString* path1 = [rootPath stringByAppendingPathComponent:outFilePath];
            
            NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:NULL];
            unsigned long long fileSize = [attributes fileSize]; // in bytes
            fileMgr = [NSFileManager defaultManager];
            if ([fileMgr fileExistsAtPath: path ] == NO){
                status = -1;
            }
            if ([fileMgr fileExistsAtPath: path1 ] == NO){
                NSLog (@"Output file does not exist, creating a new one");
                [fileMgr createFileAtPath: path1 contents: nil attributes: nil];
            }
            
            if(status!= -1){
                NSData *inputDataBuffer;
                inputFileHandle = [NSFileHandle fileHandleForReadingAtPath: path];
                NSAssert( inputFileHandle != nil, @"Failed to open handle for input file" );
                outputFileHandle  = [NSFileHandle fileHandleForWritingAtPath: path1];
                NSAssert( outputFileHandle != nil, @"Failed to open handle for output file" );                
                [inputFileHandle seekToFileOffset: 8];
                [outputFileHandle seekToFileOffset: 8];
                
                while( (inputDataBuffer = [inputFileHandle readDataOfLength: 8192]) != nil  && inputDataBuffer.length > 0)
                {
                    [outputFileHandle writeData:  inputDataBuffer];
                    inputDataBuffer = @"";
                }
            }
        }
        @catch (NSException *exception) {
            NSLog (@"Sone exception occured");
        }
        @finally{
            [inputFileHandle closeFile];
            [outputFileHandle closeFile];
        }
        
        
        if(status == 0){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:resultOk];
        }
        else{
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:resultFAIL];
        }
    }

    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void)changeOrientation:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* requiredOrientation = [command.arguments objectAtIndex:0];
    NSString* resultOk = @"OK";
    
    if([requiredOrientation isEqualToString:@"PORTRAIT"]){
        NSLog(@"trying to change the orientation to portrait");
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    }
    else{
        NSLog(@"trying to change the orientation to landscape");
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    }
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:resultOk];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end

