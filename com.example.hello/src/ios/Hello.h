#import <Cordova/CDV.h>

@interface Hello : CDVPlugin

- (void) greet:(CDVInvokedUrlCommand*)command;
- (NSString*) getImeiNumber:(CDVInvokedUrlCommand*)command;
- (void) mergeFiles:(CDVInvokedUrlCommand*)command;
- (void) changeOrientation:(CDVInvokedUrlCommand*)command;
@end
