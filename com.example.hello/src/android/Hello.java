package com.example.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import android.util.Base64;
import android.util.Log;
import android.os.Environment;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.RandomAccessFile;
import android.content.Context;
import org.apache.cordova.*;
import java.io.File;
import android.content.pm.ActivityInfo;
import android.app.Activity;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import android.telephony.TelephonyManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.provider.Settings;
import android.content.Intent;
import android.os.Build;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.ApplicationInfo;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import android.view.Window;
import android.view.WindowManager;

public class Hello extends CordovaPlugin {
    private Imei imei = null;
    private Directory directory = null;
    private MergeFileUtils mergeFileUtil = null;
    private OrientationUtil orientationUtil = null;
    private Util util = null;
    private final int BUFFER_SIZE = 8192; // 8KB
    private final int HEADER_SIZE =8;
    public static String DIR_NAME = "/Pictures/";
    public static String DOWNLOADS_DIR_NAME = "/Downloads/";
    private CryptoUtils mCryptoUtils = null;
    Context mContext;
    Activity activity;
    @Override
    public boolean execute(final String action, final JSONArray data, final CallbackContext callbackContext) throws JSONException {
	   Log.d("Vivikta_Vlearn","Action is " + action );
        if (action.equals("greet")) {

            String name = data.getString(0);
            String message = "Hello, " + name;
            callbackContext.success(message);

            return true;

        }
        else if(action.equals("getImeiNumber")) {
            activity = this.cordova.getActivity();
            if(imei == null){
                imei = new Imei();
            }
            String message = imei.getImeiNumber();
            callbackContext.success(message);
            return true;
        }
        else if(action.equals("getPublicDirectory")){
            if(directory == null){
                directory = new Directory();
            }
            String directoryPath = directory.getPublicDirectoryPath();
            callbackContext.success(directoryPath);
            return true;
        }
        else if (action.equals("mergeFiles")) {
			mContext = this.cordova.getActivity().getApplicationContext();
            
			cordova.getThreadPool().execute(new Runnable() {
				public void run() {
					String message ="";
					int status = 0;
					Log.d("Vivikta_Vlearn","Action is " + action );
					if( mergeFileUtil == null){
						Log.d("Vivikta_Vlearn","mergeFile is Null .. so creating " );
						mergeFileUtil = new MergeFileUtils();
					}
					try{
						String pathForEncryptedFile = data.getString(0);
						String pathForDecryptedFile = data.getString(1);
					
                        mergeFileUtil.merge(pathForEncryptedFile, pathForDecryptedFile);
						
                        message = "  PathForEncryptedFile :" + pathForEncryptedFile + "PathForDecryptedFile"+  pathForDecryptedFile ;
                        Log.d("Vivikta_Vlearn","Paths: "+message);
						status = 1;
						
						if(status == 1){
							callbackContext.success(message);
						}
						else{
							Log.d("Vivikta_Vlearn","Failure");
						}
					}
					catch(Exception e){
						e.printStackTrace();
					} 
					finally{
						Log.d("Vivikta_Vlearn","Inside finally");
						
					}
					//callbackContext.success(); // Thread-safe.
				}
			});
			return true;

        }
        else if (action.equals("decryptFile")) {
            
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    int status = 0;
                    FileInputStream encryptedFile = null;
                    FileOutputStream decryptedFile = null;
                    String message ="";
                    if( mCryptoUtils == null){
                        mCryptoUtils = new CryptoUtils();
                    }
 
                    try{
                        
                        String keyForDecryption = data.getString(0);
                        String pathForEncryptedFile = data.getString(1);
                        String pathForDecryptedFile = data.getString(2);
                    
                        encryptedFile = new FileInputStream(Environment.getExternalStorageDirectory() +  DIR_NAME  +pathForEncryptedFile);
                        decryptedFile = new FileOutputStream(Environment.getExternalStorageDirectory() +  DIR_NAME  +pathForDecryptedFile);
                        mCryptoUtils.decrypt(keyForDecryption, encryptedFile, decryptedFile);
                        message = "Decryption Success key Got is "+ keyForDecryption + "  PathForEncryptedFile :" + pathForEncryptedFile + "PathForDecryptedFile"+  decryptedFile ;
                        status = 1;
                        if (encryptedFile!=null)
                         {
                            encryptedFile.close(); 
                         } 
                         if (decryptedFile!=null)
                         {
                            decryptedFile.close(); 
                         } 
                         if(status==1){
                            callbackContext.success(message);
                         }
                         else{
                            //do nothing
                         }
                        

                    }catch(FileNotFoundException ex){
                        ex.printStackTrace();
                    }
                    catch(IOException ex){
                        ex.printStackTrace();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    } 
                    finally
                    { 
                        //Log.d("Vivikta_Vlearn","Inside finally ");
                        
                    }    
                    
                    //callbackContext.success(); // Thread-safe.
                }
            });
            return true;

        }
        else if(action.equals("changeOrientation")){
            activity = this.cordova.getActivity();
            if(orientationUtil == null){
                orientationUtil = new OrientationUtil();
            }
            //get the current orientation as parameter
            String currentOrientation = data.getString(0);
            Log.d("Vivikta_Vlearn","currentOrientation: "+currentOrientation);

            orientationUtil.changeOrientation(currentOrientation);

            callbackContext.success();
            return true;
        }
        else if(action.equals("isRooted")){
            if(util == null){
                util = new Util();
            }

            Boolean isRootedDevice = util.isRooted();

            callbackContext.success(isRootedDevice.toString());
            if(isRootedDevice == true){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }
    }

    public class Imei {
        public String getImeiNumber(){
            try{
                TelephonyManager telephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
                Log.d("Vivikta_Vlearn","IMEI number: "+telephonyManager.getDeviceId());
                return telephonyManager.getDeviceId();
            }
            catch(Exception e){
                Log.d("Vivikta_Vlearn","Unable to fetch IMEI number: "+e.getMessage());
            }
            return null;
        }    
    }

    public class Directory{
        public String getPublicDirectoryPath(){
            String directoryPath = Environment.getExternalStorageDirectory().toString();
            return directoryPath;
        }
    }

    public class MergeFileUtils {
        public  void merge(String inputFilePath, String outputFilePath){
		      Log.d("Vivikta_Vlearn","merge now calling actual mergeFiles Function.." );
		      try{
                mergeFiles(inputFilePath, outputFilePath);
		      }
		      catch(Exception e){
		 	    Log.d("Vivikta_Vlearn","Some exception  " );
		      }
		      Log.d("Vivikta_Vlearn","Decryption Completed exiting now safely " );
       }
        
        private void mergeFiles(String inputFilePath,String outputFilePath)  {
            Log.d("Vivikta_Vlearn","calling mergeFiles-->");
            String inputFile = (mContext.getFilesDir())+inputFilePath;
            Log.d("Vivikta_Vlearn","inputFile: "+inputFile);
            String outputFile = (mContext.getFilesDir())+outputFilePath;
            Log.d("Vivikta_Vlearn","outputFile: "+outputFile);
            File iFile = new File(inputFile);
            File oFile = new File(outputFile);
            try {
                InputStream inputStream = new BufferedInputStream(new FileInputStream(iFile));
                RandomAccessFile writerContent = new RandomAccessFile(oFile, "rw");
                Log.d("Vivikta_Vlearn","Inside try block of mergeFiles-->");
                inputStream.skip(HEADER_SIZE);
                int byteRead;
                int offset =HEADER_SIZE;

                //This will read 4096 Bytes at One Shot.
                byte[] buffer = new byte[BUFFER_SIZE];
                int noOfBytesRead =0;
                while ((noOfBytesRead = inputStream.read(buffer) )!= -1) {
                    writerContent.seek(offset);
                    writerContent.write(buffer);
                    Log.d("Vivikta_Vlearn","read bytes length equal to: "+noOfBytesRead+" content read equal to: "+buffer.toString());
                    offset+=noOfBytesRead;
                }
                writerContent.close();
                Log.d("Vivikta_Vlearn","File closed after writing..");
            }
            catch(FileNotFoundException e){
                e.printStackTrace();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
		}
	}

    public class CryptoException extends Exception {
        public CryptoException() {
        }

        public CryptoException(String message, Throwable throwable) {
        super(message, throwable);
        }
    }

    public class CryptoUtils {
        private  final String ALGORITHM = "AES";
        private  final String TRANSFORMATION = "AES";

        public  void encrypt(String key, FileInputStream inputFile, FileOutputStream outputFile)
        {
            try{
                doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
            }
            catch(Exception e){
             
            }
        }

        public  void decrypt(String key, FileInputStream inputFile, FileOutputStream outputFile)
        {
            try{
                doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
            }
            catch(Exception e){
                //Log.d("Vivikta_Vlearn","Some exception  " );
            }
            //Log.d("Vivikta_Vlearn","Decryption Compleyted exiting now safely " );
        }

        private void doCrypto(int cipherMode, String key, FileInputStream inputFile,
                                 FileOutputStream outputFile)  {
            try {
                SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
                Cipher cipher = Cipher.getInstance("AES");
                cipher.init(cipherMode, skeySpec);
                byte[] input = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputFile.read(input, 0, 4096)) != -1)
                {   
                    //Log.d("Vivikta_Vlearn","Input data is= "+input);
                    byte[] output = cipher.update(input, 0, bytesRead);
                    //Log.d("Vivikta_Vlearn","Output data is= "+output);
                    if (output != null) outputFile.write(output);
                }

            } catch (Exception e) {
                System.out.println("Error encrypting/decrypting file");
            }
        }
    }

    public class OrientationUtil{
        public void changeOrientation(String currentOrientation){
            Log.d("Vivikta_Vlearn","Trying to change the orientation...");

            if(currentOrientation.equals("PORTRAIT")){
                Log.d("Vivikta_Vlearn","trying to change the orientation to PORTRAIT");
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            else{
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }
    }
    
    public class Util{
        private boolean isRooted() {
            Log.d("Vivikta_Vlearn","Checking for rooted device..");
            return findBinary("su");
        }

        public boolean findBinary(String binaryName) {
            boolean found = false;
            if (!found) {
                String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
                        "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
                for (String where : places) {
                    if ( new File( where + binaryName ).exists() ) {
                        found = true;
                        break;
                    }
                }
            }
            return found;
        }
    }
}

